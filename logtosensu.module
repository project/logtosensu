<?php

/**
 * @file
 * Redirects logging messages to sensu.
 */

/**
 * Implements hook_help().
 */
function logtosensu_help($path, $arg) {
  switch ($path) {
    case 'admin/help#logtosensu':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t("The Logtosensu module logs events by sending messages to Sensu. It writes directly to Sensu's port, typically 3030.") . '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function logtosensu_form_system_logging_settings_alter(&$form, &$form_state) {
  $form['logtosensu_host'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Sensu host'),
    '#default_value' => variable_get('logtosensu_host', 'localhost'),
    '#description'   => t('The Logtosensu module will send events to a port on this host. Typically this is localhost.'),
  );
  $form['logtosensu_port'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Sensu port'),
    '#default_value' => variable_get('logtosensu_port', '3030'),
    '#description'   => t('The Logtosensu module will send events to this port on the Sensu host. It is expected that a Sensu client is already listening on this port. Typically this is port 3030.'),
  );
  $form['logtosensu_handlers'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Sensu handler'),
    '#default_value' => variable_get('logtosensu_handlers', 'default'),
    '#description'   => t('The name of the Sensu handler on the Sensu server that will handle events sent from Drupal. You may specify multiple handlers by separating them with commas.'),
  );
  $form['severity_settings'] = array(
    '#type' => 'fieldset',
    '#description' => t('Drupal has seven severity levels but Sensu only recognizes three: OK (nothing sent), Warning and Critical. If you would like to force watchdog messages of OK to be sent to sensu, set the severity level to Metric. Sensu will treat this as a Sensu metric and send the message rather than discarding it.')
  );
  $drupal_severity_defaults = logtosensu_severity_defaults();
  $options = array(
    0 => t('Do not send'),
    1 => t('WARNING'),
    2 => t('CRITICAL'),
    3 => t('Metric')
  );
  foreach ($drupal_severity_defaults as $severity => $default) {
    $form['severity_settings']['logtosensu_' . $severity] = array(
      '#type' => 'radios',
      '#title' => t('How should !severity entries be sent to Sensu?', array('!severity' => $severity)),
      '#options' => $options,
      '#default_value' => variable_get('logtosensu_' . $severity, $default),
    );
  }
  $form['logtosensu_test'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Send a test event to Sensu when this configuration is saved'),
    '#default_value' => variable_get('logtosensu_test', 1),
  );
  $form['#submit'][] = 'logtosensu_form_system_logging_settings_submit';
}

function logtosensu_form_system_logging_settings_submit($form, $form_state) {
  if ($form_state['values']['logtosensu_test']) {
    logtosensu_send_test_event();
  }
}

function logtosensu_send_test_event() {
  global $user, $base_root, $base_url;
  
  $log_entry = array(
    'severity'  => WATCHDOG_CRITICAL,
    'timestamp' => time(),
    'type'      => 'logtosensu',
    'link'        => '',
    'user'        => $user,
    'uid'         => $user_uid = isset($user->uid) ? $user->uid : 0,
    'request_uri' => $base_root . request_uri(),
    'referer'     => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
    'ip'          => ip_address(),
    'message'     => t('Test message'),
    'sensu_status' => 2,
  );
  logtosensu_watchdog($log_entry);
}

/**
 * Implements hook_watchdog().
 *
 * The WATCHDOG_* constant definitions correspond to the logging severity levels.
 * See includes/bootstrap.inc.
 *
 * WATCHDOG_EMERGENCY: 0
 * WATCHDOG_ALERT:     1
 * WATCHDOG_CRITICAL:  2
 * WATCHDOG_ERROR:     3
 * WATCHDOG_WARNING:   4
 * WATCHDOG_NOTICE:    5
 * WATCHDOG_INFO:      6
 * WATCHDOG_DEBUG:     7
 * 
 * A sensu status set in $log_entry['sensu_status'] overrides default configuration.
 * 
 */
function logtosensu_watchdog(array $log_entry) {
  global $base_url;
  $sensu_status = 0;
  
  $severity_defaults = logtosensu_severity_defaults();
  switch ($log_entry['severity']) {
    case WATCHDOG_EMERGENCY:
      $constant = 'WATCHDOG_EMERGENCY';
      break;
    case WATCHDOG_ALERT   :
      $constant = 'WATCHDOG_ALERT';
      break;
    case WATCHDOG_CRITICAL:
      $constant = 'WATCHDOG_CRITICAL';
      break;
    case WATCHDOG_ERROR   :
      $constant = 'WATCHDOG_ERROR';
      break;
    case WATCHDOG_WARNING :
      $constant = 'WATCHDOG_WARNING';
      break;
    case WATCHDOG_NOTICE  :
      $constant = 'WATCHDOG_NOTICE';
      break;
    case WATCHDOG_INFO    :
      $constant = 'WATCHDOG_INFO';
      break;
    case WATCHDOG_DEBUG   :
      $constant = 'WATCHDOG_DEBUG';
      break;
  }
  
  $mapped_severity = variable_get('logtosensu_' . $constant, $severity_defaults[$constant]);
  if ($mapped_severity == 0) {
    // User has chosen not to send this event.
    return;
  }
  if ($mapped_severity == 1 || $mapped_severity == 2) {
    $sensu_status = $mapped_severity;
  }
  
  $handlers = explode(',', variable_get('logtosensu_handlers', 'default'));
  array_map('trim', $handlers);
  
  $payload = array(
    'name' => 'watchdog',
    'output' => strip_tags(!isset($log_entry['variables']) ? $log_entry['message'] : strtr($log_entry['message'], $log_entry['variables'])),
    'status' => isset($log_entry['sensu_status']) ? $log_entry['sensu_status'] : $sensu_status,
    'handler' => $handlers,
    'drupal_base_url' => $base_url,
    'drupal_timestamp' => $log_entry['timestamp'],
    'drupal_type' => $log_entry['type'],
    'drupal_ip' => $log_entry['ip'],
    'drupal_request_uri' => $log_entry['request_uri'],
    'drupal_referer' => $log_entry['referer'],
    'drupal_uid' => $log_entry['uid'],
    'drupal_link' => strip_tags($log_entry['link']),
  );
  
  if ($mapped_severity == 3) {
    $payload['type'] = 'metric';
  }
  
  $json = drupal_json_encode($payload);
  logtosensu_write($json);     
}
  
/**
 * Write to sensu socket.
 *
 * @param JSON $message
 */
function logtosensu_write($message) {
  $fp = fsockopen(variable_get('logtosensu_host', 'localhost'), variable_get('logtosensu_port', 3030), $errno, $errstr, 10);
  if ($fp) {
    fwrite($fp, $message);
    fclose($fp);
  }
}

function logtosensu_severity_defaults() {
  return array(
    'WATCHDOG_EMERGENCY' => 2,
    'WATCHDOG_ALERT'     => 2,
    'WATCHDOG_CRITICAL' => 2,
    'WATCHDOG_ERROR'     => 1,
    'WATCHDOG_WARNING'   => 1,
    'WATCHDOG_NOTICE'    => 0,
    'WATCHDOG_INFO'      => 0,
    'WATCHDOG_DEBUG'     => 0,
  );
}
