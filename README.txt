-- SUMMARY --

This module flows selected Drupal watchdog events into the Sensu monitoring
system. For more about Sensu, see http://sensuapp.org.

The project page for this site is:

-- REQUIREMENTS --

Drupal must be able to write to a socket where Sensu is listening.

If you are running the Sensu client on the same host as Drupal, this is
port 3030 by default.

-- INSTALLATION --

Just like any other module. If you haven't installed a Drupal module before,
see

https://drupal.org/documentation/install/modules-themes/modules-7

-- CONFIGURATION --

Configure at Administration / Configuration / Development / Logging and Errors.
